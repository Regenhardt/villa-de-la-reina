using Microsoft.Extensions.Options;
using VillaDeLaReina.Calendar;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.Configure<CalendarSettings>(builder.Configuration.GetSection("CalendarSettings"));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Map("{*catchAll}", ProxyToOriginal);

app.Run();


static async Task ProxyToOriginal(HttpContext context)
{
    var calendarSettings = context.RequestServices.GetRequiredService<IOptions<CalendarSettings>>().Value;
    var targetUrl = context.Request.Path != "/"
        ? new Uri(calendarSettings.PropertyUri, context.Request.Path).ToString()
        : calendarSettings.PropertyUri.ToString();

    using var requestMessage = new HttpRequestMessage(HttpMethod.Get, targetUrl);

    // Copy headers from client's request to the outgoing request
    foreach (var header in context.Request.Headers)
    {
        if (!requestMessage.Headers.Contains(header.Key) && header.Key != "Host" && header.Key != "Referer")
        {
            requestMessage.Headers.TryAddWithoutValidation(header.Key, header.Value.ToArray());
        }
    }

    requestMessage.Headers.Host = calendarSettings.PropertyUri.Host;

    // Modify the Referer header if present
    if (context.Request.Headers.ContainsKey("Referer"))
    {
        requestMessage.Headers.Referrer = calendarSettings.PropertyUri;
    }

    using var client = new HttpClient();
    var response = await client.SendAsync(requestMessage);

    // Copy headers from the original response to the client's response
    foreach (var header in response.Headers)
    {
        if (header.Key != "Transfer-Encoding" && header.Key != "Content-Length")
        {
            context.Response.Headers[header.Key] = header.Value.ToArray();
        }
    }

    // Also copy content headers
    foreach (var header in response.Content.Headers)
    {
        if (header.Key != "Transfer-Encoding" && header.Key != "Content-Length")
        {
            context.Response.Headers[header.Key] = header.Value.ToArray();
        }
    }

    // Set the status code from the original response
    context.Response.StatusCode = (int)response.StatusCode;

    // Stream the content
    await response.Content.CopyToAsync(context.Response.Body);
}
