namespace VillaDeLaReina.Calendar;

using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

[ApiController]
[Route("[controller]")]
public class CalendarController : ControllerBase
{
    private readonly Uri propertyUri;
    private readonly HttpClient client = new();

    public CalendarController(IOptions<CalendarSettings> settings)
    {
        propertyUri = settings.Value.PropertyUri;
    }

    [HttpGet]
    public async Task<IActionResult> Get()
    {
        var response = await client.GetStringAsync(propertyUri);

        var doc = new HtmlDocument();
        doc.LoadHtml(response);

        // Inject custom CSS
        var headNode = doc.DocumentNode.SelectSingleNode("//head");
        if (headNode == null)
        {
            headNode = doc.CreateElement("head");
            doc.DocumentNode.PrependChild(headNode);
        }

        var styleNode = doc.CreateElement("style");
        styleNode.InnerHtml = @"
/* Your custom CSS goes here */
div:has(#reviews-section) {
    display: none !important;
}

#description-section, #distribution-section, #amenities-section, nav, #reviews-section, #location-section, iframe, .reservation_sidebar {
    display: none !important;
}

#images-section {
    display: ruby-text !important;
}

/* sibling after #location-section  */ 
#images-section ~ div {
    height: 1px !important;
}

/* div surrounding/containing a h1 element, hide the entire div */
div:has(div:has(h1)) {
    display: none !important;
}
";
        headNode.AppendChild(styleNode);

        // Extract the DayPicker node and replace the body
        //var calendarNode = doc.DocumentNode.SelectSingleNode("//*[contains(@class, 'DayPicker')]");
        //doc.DocumentNode.SelectSingleNode("//body").InnerHtml = calendarNode.OuterHtml;

        return Content(doc.DocumentNode.OuterHtml, "text/html");
    }
}
